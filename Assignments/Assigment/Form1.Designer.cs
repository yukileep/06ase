﻿namespace Assigment
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Canvas = new System.Windows.Forms.Panel();
            this.Position = new System.Windows.Forms.TextBox();
            this.CommandLine = new System.Windows.Forms.TextBox();
            this.Log = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.Canvas.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // Canvas
            // 
            this.Canvas.Controls.Add(this.Position);
            this.Canvas.Location = new System.Drawing.Point(0, 0);
            this.Canvas.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Canvas.Name = "Canvas";
            this.Canvas.Size = new System.Drawing.Size(816, 432);
            this.Canvas.TabIndex = 0;
            this.Canvas.Paint += new System.Windows.Forms.PaintEventHandler(this.Canvas_Paint);
            // 
            // Position
            // 
            this.Position.BackColor = System.Drawing.SystemColors.Control;
            this.Position.Enabled = false;
            this.Position.Location = new System.Drawing.Point(0, 401);
            this.Position.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Position.Name = "Position";
            this.Position.ReadOnly = true;
            this.Position.Size = new System.Drawing.Size(89, 22);
            this.Position.TabIndex = 0;
            this.Position.Text = "Position: 0 , 0";
            // 
            // CommandLine
            // 
            this.CommandLine.Font = new System.Drawing.Font("Microsoft JhengHei UI", 11.25F);
            this.CommandLine.Location = new System.Drawing.Point(0, 127);
            this.CommandLine.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.CommandLine.Name = "CommandLine";
            this.CommandLine.Size = new System.Drawing.Size(816, 27);
            this.CommandLine.TabIndex = 0;
            this.CommandLine.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CommandLine_KeyDown);
            // 
            // Log
            // 
            this.Log.BackColor = System.Drawing.SystemColors.InfoText;
            this.Log.Font = new System.Drawing.Font("Comic Sans MS", 12F);
            this.Log.ForeColor = System.Drawing.Color.Lime;
            this.Log.Location = new System.Drawing.Point(0, 427);
            this.Log.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Log.Multiline = true;
            this.Log.Name = "Log";
            this.Log.ReadOnly = true;
            this.Log.Size = new System.Drawing.Size(816, 132);
            this.Log.TabIndex = 1;
            this.Log.TextChanged += new System.EventHandler(this.Log_TextChanged);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.Controls.Add(this.CommandLine);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.ForeColor = System.Drawing.Color.White;
            this.panel2.Location = new System.Drawing.Point(0, 427);
            this.panel2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(813, 156);
            this.panel2.TabIndex = 3;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(813, 583);
            this.Controls.Add(this.Log);
            this.Controls.Add(this.Canvas);
            this.Controls.Add(this.panel2);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Canvas.ResumeLayout(false);
            this.Canvas.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel Canvas;
        private System.Windows.Forms.TextBox Position;
        private System.Windows.Forms.TextBox CommandLine;
        private System.Windows.Forms.TextBox Log;
        private System.Windows.Forms.Panel panel2;
    }
}

