﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using Assignment.Library;



namespace Assigment
{
    public partial class Form1 : Form
    {
        static int curX = 0, curY = 0, param1 = 0, param2 = 0, param3 = 0, param4 = 0;
        static Pen myPen = new Pen(Color.Orange, 2);
        static Pen myPos = new Pen(Color.Blue, 2); // draw moveto
        static Pen erasePos = new Pen(Color.LightSteelBlue, 2);  //erase moveto
        static Brush myBrush = new SolidBrush(Color.Orange);
        static Boolean filled = false;
        static ClassLib classLib = new ClassLib();

        private void Canvas_Paint(object sender, PaintEventArgs e)
        {

        }

        private void Log_TextChanged(object sender, EventArgs e)
        {

        }

        //reset position
        static Point origin1 = new Point(0, 0);  
        static Point origin2 = new Point(0, 0);



        public Form1()
        {
            InitializeComponent();
            this.CenterToScreen();
            Canvas.BackColor = Color.LightSteelBlue;
            Log.BackColor = Color.Black;
            

        } // constructor

        private void CommandLine_KeyDown(object sender, KeyEventArgs e)
        {


            if (e.KeyCode == Keys.Enter)
            {
                string command = CommandLine.Text;
                command = command.ToLower();
                Paint(ref Log, command);
                CommandLine.Clear(); //clear textbox buffer

            } //if

        } //KeyDown

        private void Paint(ref TextBox Log, string command)
        {

            Graphics g = null;
            g = Canvas.CreateGraphics();
            string[] commandArray = command.Split(' ');

            //Save Text File to C Drive
            SaveFileDialog Save = new SaveFileDialog();
            Save.InitialDirectory = @"C:\";
            Save.RestoreDirectory = true;
            Save.FileName = "*.txt";
            Save.DefaultExt = "txt";
            Save.Filter = "txt files (*.txt) | *.txt";

            // Read Text File from C Drive
            OpenFileDialog Open = new OpenFileDialog();
            Open.InitialDirectory = @"C:\";
            Open.Title = "Run File";
            Open.FileName = "*.txt";
            Open.DefaultExt = "txt";
            Open.Filter = "txt files (*.txt) | *.txt";

            switch (commandArray[0])
            {
                case "drawto":
                    try
                    {   // paramter error checking
                        classLib.CheckParamsThree(commandArray, ref param1, ref param2);
                        //erasePos
                        g.DrawEllipse(erasePos, curX - 5, curY - 5, 10, 10);
                        g.DrawLine(erasePos, curX - 10, curY, curX + 10, curY);
                        g.DrawLine(erasePos, curX, curY - 10, curX, curY + 10);

                        //Draw Line
                        g.DrawLine(myPen, curX, curY, param1, param2);
                        curX = param1;
                        curY = param2;

                        // current Poistion
                        g.DrawEllipse(myPos, curX - 5, curY - 5, 10, 10);
                        g.DrawLine(myPos, curX - 10, curY, curX + 10, curY);
                        g.DrawLine(myPos, curX, curY - 10, curX, curY + 10);
                        Position.Text = "Position: " + curX.ToString() + "," + curY.ToString();
                        Log.AppendText(command + Environment.NewLine);
                    }
                    catch (Exception ex)
                    {
                        CommandLine.Text = ex.Message;
                    }
                    break;
                case "moveto":
                    try
                    {   // paramter error checking
                        classLib.CheckParamsThree(commandArray, ref param1, ref param2);

                        //erasePos
                        g.DrawEllipse(erasePos, curX - 5, curY - 5, 10, 10);
                        g.DrawLine(erasePos, curX - 10, curY, curX + 10, curY);
                        g.DrawLine(erasePos, curX, curY - 10, curX, curY + 10);
                        curX = param1;
                        curY = param2;

                        // current Poistion
                        g.DrawEllipse(myPos, curX - 5, curY - 5, 10, 10);
                        g.DrawLine(myPos, curX - 10, curY, curX + 10, curY);
                        g.DrawLine(myPos, curX, curY - 10, curX, curY + 10);
                        Position.Text = "Position: " + curX.ToString() + "," + curY.ToString();
                        Log.AppendText(command + Environment.NewLine);
                    }
                    catch (Exception ex)
                    {
                        CommandLine.Text = ex.Message;
                    }
                    break;
                case "rect":
                    try
                    {   // paramter error checking
                        classLib.CheckParamsThree(commandArray, ref param1, ref param2);

                        //erasePos
                        g.DrawEllipse(erasePos, curX - 5, curY - 5, 10, 10);
                        g.DrawLine(erasePos, curX - 10, curY, curX + 10, curY);
                        g.DrawLine(erasePos, curX, curY - 10, curX, curY + 10);

                        //Draw and Fill Rectangle
                        if (filled == true)
                        {
                            g.FillRectangle(myBrush, curX, curY, param1, param2);
                        }
                        else
                        {
                            g.DrawRectangle(myPen, curX, curY, param1, param2);
                        }
                        curX = curX + param1;
                        curY = curY + param2;

                        // current Poistion
                        g.DrawEllipse(myPos, curX - 5, curY - 5, 10, 10);
                        g.DrawLine(myPos, curX - 10, curY, curX + 10, curY);
                        g.DrawLine(myPos, curX, curY - 10, curX, curY + 10);
                        Position.Text = "Position: " + curX.ToString() + "," + curY.ToString();
                        Log.AppendText(command + Environment.NewLine);
                    }
                    catch (Exception ex)
                    {
                        CommandLine.Text = ex.Message;
                    }
                    break;
                case "circ":
                    try
                    {   // paramter error checking
                        classLib.CheckParamsOne(commandArray, ref param1);

                        //erasePos
                        g.DrawEllipse(erasePos, curX - 5, curY - 5, 10, 10);
                        g.DrawLine(erasePos, curX - 10, curY, curX + 10, curY);
                        g.DrawLine(erasePos, curX, curY - 10, curX, curY + 10);

                        //Draw and Fill circle
                        if (filled == true)
                        {
                            g.FillEllipse(myBrush, curX - param1, curY - param1, param1 + param1, param1 + param1);
                        }
                        else
                        {
                            g.DrawEllipse(myPen, curX - param1, curY - param1, param1 + param1, param1 + param1);
                        }
                        // current Poistion
                        g.DrawEllipse(myPos, curX - 5, curY - 5, 10, 10);
                        g.DrawLine(myPos, curX - 10, curY, curX + 10, curY);
                        g.DrawLine(myPos, curX, curY - 10, curX, curY + 10);
                        Position.Text = "Position: " + curX.ToString() + "," + curY.ToString();
                        Log.AppendText(command + Environment.NewLine);
                    }
                    catch (Exception ex)
                    {
                        CommandLine.Text = ex.Message;
                    }
                    break;
                case "tri":
                    try
                    {   // paramter error checking
                        classLib.CheckParamsFour(commandArray, ref param1, ref param2, ref param3, ref param4);

                        //erasePos
                        g.DrawEllipse(erasePos, curX - 5, curY - 5, 10, 10);
                        g.DrawLine(erasePos, curX - 10, curY, curX + 10, curY);
                        g.DrawLine(erasePos, curX, curY - 10, curX, curY + 10);

                        // Draw triangle
                        g.DrawLine(myPen, curX, curY, param1, param2);
                        g.DrawLine(myPen, curX, curY, param3, param4);
                        g.DrawLine(myPen, param3, param4, param1, param2);

                        // current Poistion
                        curX = param3;
                        curY = param4;
                        g.DrawEllipse(myPos, curX - 5, curY - 5, 10, 10);
                        g.DrawLine(myPos, curX - 10, curY, curX + 10, curY);
                        g.DrawLine(myPos, curX, curY - 10, curX, curY + 10);
                        Position.Text = "Position: " + curX.ToString() + "," + curY.ToString();
                        Log.AppendText(command + Environment.NewLine);
                    }
                    catch (Exception ex)
                    {
                        CommandLine.Text = ex.Message;
                    }
                    break;
                case "run": // run command and show up graphics on Canvas again

                    var prog = Log.Text;
                    Log.Text = "";
                    var Lines = prog.Split(new[] { Environment.NewLine }, StringSplitOptions.None);
                    foreach (var line in Lines)
                    {
                        Paint(ref Log, line);
                    }
                    //CommandLine.Clear();
                    break;
                case "clear":
                    // clear Canvas
                    g.Clear(Color.LightSteelBlue);
                    break;
                case "pen":
                    Log.Text += command + Environment.NewLine;
                    Log.Text += "Change Pen Color :" + commandArray[1] + Environment.NewLine;
                    myPen.Color = Color.FromName(commandArray[1]);
                    break;
                case "save":
                    if (Save.ShowDialog() == DialogResult.OK)
                    {
                        Stream fileStrean = Save.OpenFile();
                        StreamWriter sw = new StreamWriter(fileStrean);
                        sw.WriteLine(Log.Text);
                        sw.Close();
                        fileStrean.Close();
                        g.Clear(Color.LightSteelBlue);

                        g.DrawEllipse(myPos, curX - 5, curY - 5, 10, 10);
                        g.DrawLine(myPos, curX - 10, curY, curX + 10, curY);
                        g.DrawLine(myPos, curX, curY - 10, curX, curY + 10);
                        Position.Text = "Position: " + curX.ToString() + "," + curY.ToString();
                        
                        CommandLine.Clear();
                    } // save file to C Drive
                    break;
                case "read":
                    if (Open.ShowDialog() == DialogResult.OK)
                    {
                        Stream fileStrean = Open.OpenFile();
                        StreamReader sread = new StreamReader(File.OpenRead(Open.FileName));
                        Log.Text = sread.ReadToEnd();
                        sread.Dispose();
                        CommandLine.Clear();
                    } // load text file 
                    break;
                case "reset":
                    //Canvas.Refresh();
                    g.DrawEllipse(erasePos, curX - 5, curY - 5, 10, 10);
                    g.DrawLine(erasePos, curX - 10, curY, curX + 10, curY);
                    g.DrawLine(erasePos, curX, curY - 10, curX, curY + 10);
                    curX = 0;
                    curY = 0;
                    Position.Text = "Position: " + curX.ToString() + "," + curY.ToString();
                    Log.Clear();
                    break;
                case "exit":
                    if (System.Windows.Forms.Application.MessageLoop)
                    {
                        //WinForms app
                        System.Windows.Forms.Application.Exit();
                    }
                    break;
                case "fill":
                    Log.Text += command + Environment.NewLine;
                    switch (commandArray[1])
                    {
                        case "on":
                            filled = true;

                            break;
                        case "off":
                            filled = false;
                            break;
                    } // fill switch
                    break;
                default:
                    MessageBox.Show("error input");
                    break;
            }  //switch    


        }
    }
}
