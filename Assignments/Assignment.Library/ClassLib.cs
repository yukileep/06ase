﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment.Library
{
    public class ClassLib
    {
        public Boolean CheckParamsOne(string[] command, ref int arg1)
        {
            if (command.Length != 2)
            {
                throw new Exception("Command paramters error");
                return false;
            }
            else if (!int.TryParse(command[1], out arg1))
            {
                throw new Exception("Command paramters 1 error");
                return false;
            }
            return true;
        }//CheckParamsOne

        public Boolean CheckParamsThree(string[] command, ref int arg1, ref int arg2)
        {
            if (command.Length != 3)
            {
                throw new Exception("Command paramters error");
                return false;
            }
            else if (!int.TryParse(command[1], out arg1))
            {
                throw new Exception("Command paramters 1 error");
                return false;
            }
            else if (!int.TryParse(command[2], out arg2))
            {
                throw new Exception("Command paramters 2 error");
                return false;
            }
            return true;
        } //method 
        public Boolean CheckParamsFour(string[] command, ref int arg1, ref int arg2, ref int arg3, ref int arg4)
        {
            if (command.Length != 5)
            {
                throw new Exception("Command paramters error");
                return false;
            }
            else if (!int.TryParse(command[1], out arg1))

            {
                throw new Exception("Command paramters 1 error");
                return false;
            }
            else if (!int.TryParse(command[2], out arg2))
            {
                throw new Exception("Command paramters 2 error");
                return false;
            }
            else if (!int.TryParse(command[3], out arg3))
            {
                throw new Exception("Command paramters 3 error");
                return false;
            }
            else if (!int.TryParse(command[4], out arg4))
            {
                throw new Exception("Command paramters 4 error");
                return false;
            }
            return true;
        } //method 
    }
}