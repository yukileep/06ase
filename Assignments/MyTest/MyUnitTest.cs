﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Assignment.Library;

namespace MyTest
{
    [TestClass]
    public class MyUnitTest
    {
        [TestMethod]
        public void Test_CheckParamsOne()
        {

            //Arrange
            ClassLib cl = new ClassLib();
            string[] CheckArray1 = { "circle", "100" };
            int CheckInt = 100;
            Boolean expected = true;

            //Act
            Boolean CheckError1 = cl.CheckParamsOne(CheckArray1, ref CheckInt);

            //Assert
            Assert.AreEqual(expected, CheckError1);

        }
        [TestMethod]
        public void Test_CheckParamsThree()
        {

            //Arrange
            ClassLib c2 = new ClassLib();
            string[] CheckArray = { "moveto", "100", "200" };
            int CheckInt1 = 100;
            int CheckInt2 = 200;
            Boolean expected = true;

            //Act
            Boolean CheckError2 = c2.CheckParamsThree(CheckArray, ref CheckInt1, ref CheckInt2);

            //Assert
            Assert.AreEqual(expected, CheckError2);

        }
        [TestMethod]
        public void Test_CheckParamsFour()
        {

            //Arrange
            ClassLib c2 = new ClassLib();
            string[] CheckArray = { "traingle", "100", "200", "300", "400" };
            int CheckInt1 = 100;
            int CheckInt2 = 200;
            int CheckInt3 = 300;
            int CheckInt4 = 500;
            Boolean expected = true;

            //Act
            Boolean CheckError2 = c2.CheckParamsFour(CheckArray, ref CheckInt1, ref CheckInt2, ref CheckInt3, ref CheckInt4);

            //Assert
            Assert.AreEqual(expected, CheckError2);

        }
    }
}
