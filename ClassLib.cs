﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment.Library
{/// <summary>
/// This is class library to check input correct or false
/// </summary>
    public class ClassLib
    {
        /// <summary>
        /// This is Check Params One 
        /// </summary>
        /// <param name="command">command </param>
        /// <param name="arg1">arg1</param>
        /// <returns></returns>
        public Boolean CheckParamsOne(string[] command, ref int arg1)
        {
            if (command.Length != 2)
            {
                throw new Exception("Command paramters error");
                return false;
            }
            else if (!int.TryParse(command[1], out arg1))
            {
                throw new Exception("Command paramters 1 error");
                //return false;
            }
            return true;
        }//CheckParamsOne

        /// <summary>
        /// This is CheckParamsThree
        /// </summary>
        /// <param name="command">command</param>
        /// <param name="arg1">arg1</param>
        /// <param name="arg2">arg2</param>
        /// <returns></returns>
        public Boolean CheckParamsThree(string[] command, ref int arg1, ref int arg2)
        {
            if (command.Length != 3)
            {
                throw new Exception("Command paramters error");
                return false;
            }
            else if (!int.TryParse(command[1], out arg1))
            {
                throw new Exception("Command paramters 1 error");
                return false;
            }
            else if (!int.TryParse(command[2], out arg2))
            {
                throw new Exception("Command paramters 2 error");
                return false;
            }
            return true;
        } //method 
        /// <summary>
        /// This is CheckParamsfour
        /// </summary>
        /// <param name="command">command</param>
        /// <param name="arg1">arg1</param>
        /// <param name="arg2">arg2</param>
        /// <param name="arg3">arg3</param>
        /// <param name="arg4">arg4</param>
        /// <returns></returns>
        public Boolean CheckParamsFour(string[] command, ref int arg1, ref int arg2, ref int arg3, ref int arg4)
        {
            if (command.Length != 5)
            {
                throw new Exception("Command paramters error");
                return false;
            }
            else if (!int.TryParse(command[1], out arg1))

            {
                throw new Exception("Command paramters 1 error");
                return false;
            }
            else if (!int.TryParse(command[2], out arg2))
            {
                throw new Exception("Command paramters 2 error");
                return false;
            }
            else if (!int.TryParse(command[3], out arg3))
            {
                throw new Exception("Command paramters 3 error");
                return false;
            }
            else if (!int.TryParse(command[4], out arg4))
            {
                throw new Exception("Command paramters 4 error");
                return false;
            }
            return true;
        } //method 
    }
}