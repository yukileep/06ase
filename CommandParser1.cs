﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assigment
{
    class CommandParser1
    {
        public void CheckParams1(string[] command, ref int arg1)
        {
            if (command.Length != 2)
            {
                throw new Exception("Command paramters error");
            }
            else if (!int.TryParse(command[1], out arg1))
            {
                throw new Exception("Command paramters 1 error");
            }
        }
    }
}
