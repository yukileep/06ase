﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assigment
{
    public enum ShapeType
    {
        Line,        // 1
        Circle,      // 2
        Rectangle,   // 3
        Triangle     // 4        
    } // enum

    class Factory
    {
        //static void main (string[] args
        public Factory()
        {
            IShapeFactory shapeFactory = new ShapeFactory();
            AShape aShape = shapeFactory.CreateShape(ShapeType.Line);
            aShape = shapeFactory.CreateShape(ShapeType.Circle);
            aShape = shapeFactory.CreateShape(ShapeType.Rectangle);
            aShape = shapeFactory.CreateShape(ShapeType.Triangle);
            aShape = shapeFactory.CreateShape(ShapeType.Line);
        }
    }// class

    interface IShapeFactory
    {
        AShape CreateShape(ShapeType shape);
    } // interface

    class ShapeFactory : IShapeFactory
    {
        public AShape CreateShape(ShapeType shape)
        {
            switch (shape)
            {
                case ShapeType.Line:
                    return new Line();
                case ShapeType.Circle:
                    return new Circle();
                case ShapeType.Rectangle:
                    return new Rectangle();
                case ShapeType.Triangle:
                    return new Triangle();
                default:
                    return new Line();
            } //switch     
        } // CreateShape
    }// class

    public abstract class AShape
    {
        public string Draw { get; set; }
        public string Colour { get; set; }
    } // abstract

    public class Circle : AShape
    {
        public Circle()
        {
            Draw = "Draw";
            Colour = "Colour";
        } // constructor   
        public string Pen()
        {
            return "Draw=" + Draw + " Color=" + Colour;
        }
        public string Canvas()
        {
            return "Draw=" + Draw + " Color=" + Colour;
        }
    }
    public class Rectangle : AShape
    {
        public Rectangle()
        {
            Draw = "Draw";
            Colour = "Colour";
        } // constructor

        public string Pen()
        {
            return "Draw=" + Draw + " Color=" + Colour;
        }

        public string Canvas()
        {
            return "Draw=" + Draw + " Color=" + Colour;
        }
    }
    public class Triangle : AShape
    {
        public Triangle()
        {
            Draw = "Draw";
            Colour = "Colour";

        } // constructor

        public string Pen()
        {
            return "Draw=" + Draw + " Color=" + Colour;
        }

        public string Canvas()
        {
            return "Draw=" + Draw + " Color=" + Colour;
        }
    } // class

    public class Line : AShape
    {
        public Line()
        {
            Draw = "Draw";
            Colour = "Colour";

        } // constructor 

        public string Pen()
        {
            return "Draw=" + Draw + " Color=" + Colour;
        }

        public string Canvas()
        {
            return "Draw=" + Draw + " Color=" + Colour;
        }
    }
}

