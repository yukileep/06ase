﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using Assignment.Library;

namespace Assigment
{
    public partial class Form1 : Form
    {
        static int curX = 0, curY = 0, param1 = 0, param2 = 0, param3 = 0, param4 = 0;
        static Pen myPen = new Pen(Color.Orange, 2);
        static Pen myPos = new Pen(Color.Blue, 2); // draw moveto
        static Pen erasePos = new Pen(Color.LightSteelBlue, 2);  //erase moveto
        static Brush myBrush = new SolidBrush(Color.Orange);
        static Boolean filled = false;
        static ClassLib classLib = new ClassLib();
        //static string Line;
        Dictionary<string, int> MyDic = new Dictionary<string, int>();

        private void Canvas_Paint(object sender, PaintEventArgs e)
        {

        }

        private void Log_TextChanged(object sender, EventArgs e)
        {

        }

        private void Position_TextChanged(object sender, EventArgs e)
        {

        }

        //reset position
        static Point origin1 = new Point(0, 0);  
        static Point origin2 = new Point(0, 0);
        private string inputCommand;

        public Form1()
        {
            InitializeComponent();
            this.CenterToScreen();
            Canvas.BackColor = Color.LightSteelBlue;
            Log.BackColor = Color.Black;
        } // constructor

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void CommandLine_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                string command = CommandLine.Text;
                command = command.ToLower();
                Paint(ref Log, command);
                CommandLine.Clear(); //clear textbox buffer
            } //if
        } //KeyDown


        private String FindInDicatonary(string FindMe)
        {
            if (true == (MyDic.ContainsKey(FindMe)))
            {
                return MyDic[FindMe].ToString();
            }
            else
            {
                throw new Exception("Variable Not Found");       
            }   
        }
        private void Paint(ref TextBox Log, string command)
        {
            Graphics g = null;
            g = Canvas.CreateGraphics();
            string[] commandArray = command.Split(' ');
            
            #region save file
            //Save Text File to C Drive
            SaveFileDialog Save = new SaveFileDialog();
            Save.InitialDirectory = @"C:\Users\yuki\Desktop";
            Save.RestoreDirectory = true;
            Save.FileName = "command.txt";
            Save.DefaultExt = "txt";
            Save.Filter = "txt files (*.txt) | *.txt";
            #endregion

            #region read file
            //Read Text File from C Drive
            OpenFileDialog Open = new OpenFileDialog();
            Open.InitialDirectory = @"C:\Users\yuki\Desktop";
            Open.Title = "Run File";
            Open.FileName = "command.txt";
            Open.DefaultExt = "txt";
            Open.Filter = "txt files (*.txt) | *.txt";
            #endregion

            switch (commandArray[0])
            {
                #region drawto
                case "drawto":
                    try
                    {   // paramter error checking
                        classLib.CheckParamsThree(commandArray, ref param1, ref param2);

                        //erasePos
                        g.DrawEllipse(erasePos, curX - 5, curY - 5, 10, 10);
                        g.DrawLine(erasePos, curX - 10, curY, curX + 10, curY);
                        g.DrawLine(erasePos, curX, curY - 10, curX, curY + 10);

                        //Draw Line
                        g.DrawLine(myPen, curX, curY, param1, param2);
                        curX = param1;
                        curY = param2;

                        // current Poistion
                        g.DrawEllipse(myPos, curX - 5, curY - 5, 10, 10);
                        g.DrawLine(myPos, curX - 10, curY, curX + 10, curY);
                        g.DrawLine(myPos, curX, curY - 10, curX, curY + 10);
                        Position.Text = "Position: " + curX.ToString() + "," + curY.ToString();
                        Log.AppendText(command + Environment.NewLine);
                    }
                    catch (Exception ex)
                    {
                        CommandLine.Text = ex.Message;
                    }
                    break;
                #endregion              
                #region moveto
                case "moveto":
                    try
                    {   // paramter error checking
                        classLib.CheckParamsThree(commandArray, ref param1, ref param2);
                        
                        //erasePos
                        g.DrawEllipse(erasePos, curX - 5, curY - 5, 10, 10);
                        g.DrawLine(erasePos, curX - 10, curY, curX + 10, curY);
                        g.DrawLine(erasePos, curX, curY - 10, curX, curY + 10);
                        curX = param1;
                        curY = param2;

                        // current Poistion
                        g.DrawEllipse(myPos, curX - 5, curY - 5, 10, 10);
                        g.DrawLine(myPos, curX - 10, curY, curX + 10, curY);
                        g.DrawLine(myPos, curX, curY - 10, curX, curY + 10);
                        Position.Text = "Position: " + curX.ToString() + "," + curY.ToString();
                        Log.AppendText(command + Environment.NewLine);
                    }
                    catch (Exception ex)
                    {
                        CommandLine.Text = ex.Message;
                    }
                    break;
                #endregion
                #region define
                case "define":
                    string var1 = commandArray[1];
                    param1 = Int32.Parse(commandArray[2]);
                    MyDic.Add(commandArray[1], param1);
                    Log.AppendText(command + Environment.NewLine);
                    break;
                #endregion
                #region variable
                case "variable":
                    try
                    {
                        string MyDic1 = commandArray[1];
                        string var1Value = FindInDicatonary(MyDic1);
                        MessageBox.Show("The value of " + MyDic1 + " is " + var1Value);
                    }
                    catch (Exception ex)
                    {
                        CommandLine.Text = ex.Message;
                    }
                    break;
                #endregion
                #region loop
                case "loop":
                    int loopTime = Int32.Parse(commandArray[1]);
                    int pFrom = inputCommand.IndexOf("loop ") + "loop".Length + loopTime - 1;
                    int pTo = inputCommand.LastIndexOf("endloop");
                    string oCommand = inputCommand.Substring(pFrom, pTo - pFrom);
                    // set string between loop and endloop

                    string loopCommand = Regex.Replace(oCommand, @"^\s*$\n", string.Empty, RegexOptions.Multiline).TrimEnd();
                    // remove blank line form string

                    var progLines = loopCommand.Split(new[] { Environment.NewLine }, StringSplitOptions.None);
                    //Split line of string

                    for (int loopRun = 1; loopRun < loopTime; loopRun++)
                    {
                        foreach (var FileLine in progLines)
                        {
                            Paint(ref Log, FileLine);
                        }
                    }
                    break;
                #endregion
                #region rectangle
                case "rectangle":
                    try
                    {   // paramter error checking
                        classLib.CheckParamsThree(commandArray, ref param1, ref param2);

                        //erasePos
                        g.DrawEllipse(erasePos, curX - 5, curY - 5, 10, 10);
                        g.DrawLine(erasePos, curX - 10, curY, curX + 10, curY);
                        g.DrawLine(erasePos, curX, curY - 10, curX, curY + 10);

                        //Draw and Fill Rectangle
                        if (filled == true)
                        {
                            g.FillRectangle(myBrush, curX, curY, param1, param2);
                        }
                        else
                        {
                            g.DrawRectangle(myPen, curX, curY, param1, param2);
                        }
                        curX = curX + param1;
                        curY = curY + param2;

                        // current Poistion
                        g.DrawEllipse(myPos, curX - 5, curY - 5, 10, 10);
                        g.DrawLine(myPos, curX - 10, curY, curX + 10, curY);
                        g.DrawLine(myPos, curX, curY - 10, curX, curY + 10);
                        Position.Text = "Position: " + curX.ToString() + "," + curY.ToString();
                        Log.AppendText(command + Environment.NewLine);
                    }
                    catch (Exception ex)
                    {
                        CommandLine.Text = ex.Message;
                    }
                    break;
                #endregion
                #region circle
                case "circle":
                    try
                    {   // paramter error checking
                        //classLib.CheckParamsOne(commandArray, ref param1);

                        //erasePos
                        g.DrawEllipse(erasePos, curX - 5, curY - 5, 10, 10);
                        g.DrawLine(erasePos, curX - 10, curY, curX + 10, curY);
                        g.DrawLine(erasePos, curX, curY - 10, curX, curY + 10);

                        //Draw and Fill circle
                        if (filled == true)
                        {
                            g.FillEllipse(myBrush, curX - param1, curY - param1, param1 + param1, param1 + param1);
                        }
                        else
                        {
                            g.DrawEllipse(myPen, curX - param1, curY - param1, param1 + param1, param1 + param1);
                        }
                        // current Poistion
                        g.DrawEllipse(myPos, curX - 5, curY - 5, 10, 10);
                        g.DrawLine(myPos, curX - 10, curY, curX + 10, curY);
                        g.DrawLine(myPos, curX, curY - 10, curX, curY + 10);
                        Position.Text = "Position: " + curX.ToString() + "," + curY.ToString();
                        Log.AppendText(command + Environment.NewLine);
                    }
                    catch (Exception ex)
                    {
                        CommandLine.Text = ex.Message;
                    }
                    break;
                #endregion
                #region triangle
                case "triangle":
                    try
                    {   // paramter error checking
                        classLib.CheckParamsFour(commandArray, ref param1, ref param2, ref param3, ref param4);

                        //erasePos
                        g.DrawEllipse(erasePos, curX - 5, curY - 5, 10, 10);
                        g.DrawLine(erasePos, curX - 10, curY, curX + 10, curY);
                        g.DrawLine(erasePos, curX, curY - 10, curX, curY + 10);

                        // Draw triangle
                        g.DrawLine(myPen, curX, curY, param1, param2);
                        g.DrawLine(myPen, curX, curY, param3, param4);
                        g.DrawLine(myPen, param3, param4, param1, param2);

                        // current Poistion
                        curX = param3;
                        curY = param4;
                        g.DrawEllipse(myPos, curX - 5, curY - 5, 10, 10);
                        g.DrawLine(myPos, curX - 10, curY, curX + 10, curY);
                        g.DrawLine(myPos, curX, curY - 10, curX, curY + 10);
                        Position.Text = "Position: " + curX.ToString() + "," + curY.ToString();
                        Log.AppendText(command + Environment.NewLine);
    
                    }
                    catch (Exception ex)
                    {
                        CommandLine.Text = ex.Message;
                    }
                    break;
                #endregion
                #region run
                case "run": // run command and show up graphics on Canvas again

                    var prog = Log.Text;
                    Log.Text = "";
                    var Lines = prog.Split(new[] { Environment.NewLine }, StringSplitOptions.None);
                    foreach (var line in Lines)
                    {
                        Paint(ref Log, line);
                    }
                    CommandLine.Clear();
                    
                    break;
                #endregion
                #region clear
                case "clear":
                    // clear Canvas
                    g.Clear(Color.LightSteelBlue);
                    break;
                #endregion
                #region pen
                case "pen":
                    Log.Text += command + Environment.NewLine;
                    Log.Text += "Change Pen Color :" + commandArray[1] + Environment.NewLine;
                    myPen.Color = Color.FromName(commandArray[1]);
                    break;
                #endregion
                #region save
                case "save":
                    if (Save.ShowDialog() == DialogResult.OK)
                    {
                        Stream fileStrean = Save.OpenFile();
                        StreamWriter sw = new StreamWriter(fileStrean);
                        sw.WriteLine(Log.Text);
                        sw.Close();
                        fileStrean.Close();
                        g.Clear(Color.LightSteelBlue);

                        g.DrawEllipse(myPos, curX - 5, curY - 5, 10, 10);
                        g.DrawLine(myPos, curX - 10, curY, curX + 10, curY);
                        g.DrawLine(myPos, curX, curY - 10, curX, curY + 10);
                        Position.Text = "Position: " + curX.ToString() + "," + curY.ToString();
                        
                        CommandLine.Clear();
                    } // save file to C Drive
                    break;
                #endregion

                #region read
                case "read":
                    if (Open.ShowDialog() == DialogResult.OK)
                    {
                        Stream fileStrean = Open.OpenFile();
                        StreamReader sread = new StreamReader(File.OpenRead(Open.FileName));
                        Log.Text = sread.ReadToEnd();
                        sread.Dispose();
                        CommandLine.Clear();

                    } // load text file 

                    break;

                #endregion

                #region reset
                case "reset":
                    //Canvas.Refresh();
                    g.DrawEllipse(erasePos, curX - 5, curY - 5, 10, 10);
                    g.DrawLine(erasePos, curX - 10, curY, curX + 10, curY);
                    g.DrawLine(erasePos, curX, curY - 10, curX, curY + 10);
                    curX = 0;
                    curY = 0;
                    Position.Text = "Position: " + curX.ToString() + "," + curY.ToString();
                    Log.Clear();
                    break;
                #endregion
                #region exit
                case "exit":
                    if (System.Windows.Forms.Application.MessageLoop)
                    {
                        //WinForms app
                        System.Windows.Forms.Application.Exit();
                    }
                    break;
                #endregion
                #region fill
                case "fill":
                    Log.Text += command + Environment.NewLine;
                    switch (commandArray[1])
                    {
                        case "on":
                            filled = true;

                            break;
                        case "off":
                            filled = false;
                            break;
                    } // fill switch
                    break;
                #endregion
                default:
                    MessageBox.Show("error input");
                    break;
            }  //switch    
        }//Paint


        private void Run_click(object sender, EventArgs e)
        {
            string fileLineText = "";
            inputCommand = CommandLine.Text;
            var prog = CommandLine.Text;
            var progLines = prog.Split(new[] { Environment.NewLine }, StringSplitOptions.None);
            foreach (var fileLine in progLines)
            {
                fileLineText = fileLine;
            }
        }


    }//Form1
}//namespace
